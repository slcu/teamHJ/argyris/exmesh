module Vis3D where

import Linear
import Mesh
import Vis (VisObject, Color)
import Control.Lens
import qualified Vis as VS
import qualified Data.Vector as V
import qualified Data.Vector.Storable as VS


mkCellCol :: (Int -> Color) -> Label -> Color
mkCellCol fc (Label i) = fc i
mkCellCol fc NoLabel = fc 0

scaleV :: (Fractional a) => (a, a, a) -> V3 a  -> V3 a
scaleV (xs, ys, zs) (V3 x y z) = V3 (x / xs) (y / ys) (z / zs)

data Range = Range {low :: Float, up :: Float} deriving (Show)

tys (Mesh.Triangle p1 p2 p3) = [p1 ^._y, p2 ^._y, p3 ^._y]
txs (Mesh.Triangle p1 p2 p3) = [p1 ^._x, p2 ^._x, p3 ^._x]
tzs (Mesh.Triangle p1 p2 p3) = [p1 ^._z, p2 ^._z, p3 ^._z]


bounds :: Tissue s -> (Range, Range, Range)
bounds ts = (rx, ry, rz)
  where
    ys = concat [tys (cel cell) | cell <- cells ts]
    xs = concat [txs (cel cell) | cell <- cells ts]
    zs = concat [tzs (cel cell) | cell <- cells ts]

    rx = Range {low=minimum xs, up=maximum xs}
    ry = Range {low=minimum ys, up=maximum ys}
    rz = Range {low=minimum zs, up=maximum zs}

normF :: Range -> Float -> Float
normF (Range {low = mn, up = mx}) val = (val - mn) / (mx - mn)

normV3 :: (Range, Range, Range) -> V3 Float -> V3 Float
normV3 (rx, ry, rz) (V3 x y z) = V3 (normF rx x) (normF ry y) (normF rz z)

normalise :: (Range, Range, Range) -> VisObject Float -> VisObject Float
normalise (rx, ry, rz) (VS.Triangle p1 p2 p3 cl) =
  VS.Triangle
    (normV3 (rx, ry, rz) p1)
    (normV3 (rx, ry, rz) p2)
    (normV3 (rx, ry, rz) p3)
    cl

visCell :: (Int -> Color) -> Cell s -> VisObject Float
visCell fc Cell {cel = Triangle p1 p2 p3, label = lb@(Label i), st = _} =
  VS.Triangle p1 p2 p3 VS.blue

visLabels :: (Int -> Color) -> Tissue s -> VisObject Float
visLabels fc ts =
  VS.VisObjects [(normalise rs) (visCell fc cell) | cell <- cells ts]
  where
    rs = bounds ts


showVis :: VisObject Float -> IO ()
showVis = VS.display VS.defaultOpts

assingCols :: Int -> Color
assingCols 1 = VS.blue
assignCols 2 = VS.red
assignCols _ = VS.green

instance (Show a) => Show (VisObject a) where
  show (VS.Triangle p1 p2 p3 cl) = show p1 ++ " " ++ show p2 ++ " " ++ show p3
  show (VS.VisObjects vs) = concatMap show vs


----- test
-- drawFun :: VisObject Double
-- drawFun = VisObjects $ [axes,box,ellipsoid,sphere] ++ (map text [-5..5]) ++ [boxText, plane] 
--   where
--     x = -1
--     quat = normalize $ Quaternion 1 (V3 2 3 4)
    
--     axes = Axes (0.5, 15)
--     sphere = Trans (V3 0 x (-1)) $ Sphere 0.15 Wireframe (makeColor 0.2 0.3 0.8 1)
--     ellipsoid = Trans (V3 x 0 (-1)) $ RotQuat quat $ Ellipsoid (0.2, 0.3, 0.4) Solid (makeColor 1 0.3 0.5 1)
--     box = Trans (V3 0 0 x) $ RotQuat quat $ Box (0.2, 0.2, 0.2) Wireframe (makeColor 0 1 1 1)
--     plane = Plane (V3 0 0 1) (makeColor 1 1 1 1) (makeColor 0.4 0.6 0.65 0.4)
--     text k = Text2d "OLOLOLOLOLO" (100,500 - k*100*x) TimesRoman24 (makeColor 0 (0.5 + x'/2) (0.5 - x'/2) 1)
--       where
--         x' = realToFrac $ (x + 1)/0.4*k/5
--     boxText = Text3d "trololololo" (V3 0 0 (x-0.2)) TimesRoman24 (makeColor 1 0 0 1)

-- go' :: IO ()
-- go' = do
--   display (defaultOpts {VS.optWindowName = "display test"}) drawFun

go = do
   tissue <- readGeometry "/home/argyris/home3/genes2shape/flowerMeshes/flowerAtlas/t40/t40_domains.txt"
   let tissue' = labelCond (aboveY 250) tissue
   let vis = visLabels assignCols tissue'
   showVis vis
                        
                        
                        
                        
                        
                        

